<?php

namespace JournoLink\SparkpostDriver;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class SparkpostServiceProvider extends ServiceProvider
{
    public function boot(): void
    {

    }

    public function register(): void
    {
        if (method_exists($this, $method = sprintf('registerFor%d', $this->majorVersion()))) {
            $this->{$method}();
        }
    }

    private function registerFor6(): void
    {
        $this->app->extend('swift.transport', function(\Illuminate\Mail\TransportManager $manager) {
            return $this->registerDriver($manager);
        });
    }

    private function registerFor7(): void
    {
        $this->app->extend('swift.transport', function(\Illuminate\Mail\TransportManager $manager) {
            return $this->registerDriver($manager);
        });
    }

    private function registerFor8(): void
    {
        $this->app->extend('mail.manager', function (\Illuminate\Mail\MailManager $manager) {
            return $this->registerDriver($manager);
        });
    }

    /**
     * Register the driver against the manager.
     *
     * @param $instance
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function registerDriver($instance)
    {
        $instance->extend('sparkpost', function () {
            $config = config('services.sparkpost', []);
            $sparkpostOptions = $config['options'] ?? [];
            $guzzleOptions = $config['guzzle'] ?? [];
            $client = $this->app->make(Client::class, $guzzleOptions);

            return new SparkpostDriver($client, $config['secret'], $sparkpostOptions);
        });

        return $instance;
    }

    /**
     * Return the major Laravel version of this installation.
     *
     * @return int
     */
    private function majorVersion(): int
    {
        return (int) substr($this->app->version(), 0, strpos($this->app->version(), '.'));
    }
}

# Laravel Sparkpost Mail Driver

Adds native Sparkpost support for Laravel 6.x, 7.x and 8.x

## Install

```bash
composer require journolink/laravel-sparkpost-driver
```

The Service Provider will be automatically registered with the application.
